﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
	public float speed = 4.0f;
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right;
	// Use this for initialization
	void Start () {

	}
	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		Gizmos.DrawRay(transform.position, direction1);
	}

	// Update is called once per frame

	void Update () {
		
		// get the vector from the bee to the target 
		Vector2 direction;
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		if (direction1.magnitude > direction2.magnitude) {
			 direction = direction2;
		} else {
			 direction = direction1;
		}
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);

	}
}
